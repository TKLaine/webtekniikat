'use strict'
let notes = new Array();
loadList();

function addItem() {
	let textbox = document.getElementById('item');
	let itemText = textbox.value;
	textbox.value = '';
	textbox.focus();
	let newItem = {title: itemText, quantity: 1};
	if(notes.length != 0 && newItem.title.localeCompare('') != 0) {
		for (let i = 0; i < notes.length; i++) {
			if (notes[i].title.localeCompare(newItem.title) == 0) {
				notes[i].quantity++;
				break;
			} else if (i == notes.length - 1) {
				notes.push(newItem);
				break;
			}
		}
	}else if(newItem.title.localeCompare('') != 0){
		notes.push(newItem);
	}
	saveList();
	displayList();
}

function displayList() {
	let table = document.getElementById('list');
	table.innerHTML = '';
	for (let i = 0; i<notes.length; i++) {
		let note = notes[i];
		let node = document.createElement('tr');
		let html = '<td>'+note.title+'</td><td>'+note.quantity+'</td><td><a href="#" onClick="deleteIndex('+i+')">delete</td>';
	    node.innerHTML = html;
		table.appendChild(node);
	}
}

function deleteIndex(i) {
	notes.splice(i, 1);
	saveList();
	displayList();
}

function saveList() {
    localStorage.notes = JSON.stringify(notes);
}

function loadList() {
    console.log('loadList');
    if (localStorage.notes) {
        notes = JSON.parse(localStorage.notes);
        displayList();
    }
}

let button = document.getElementById('add');
button.onclick = addItem;
