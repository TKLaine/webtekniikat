
let request = new XMLHttpRequest();
request.open('GET', 'data/books.json', false);
request.send(null);
let data = JSON.parse(request.responseText);
console.log(data);

let books = data.books;
let bookTitle = document.createElement("h1");
document.body.appendChild(bookTitle);
let table = document.createElement("table");
let tableHeading = document.createElement("th");
for (let i=0; i < books.length; i++) {
	console.log(books[i].title);
	let item = document.createElement("td");
	item.innerHTML = books[i].title + ' (' + books[i].year + ')';
	item.onclick = function(){displayName(this);};
	tableHeading.appendChild(item);
}
table.appendChild(tableHeading);
document.body.appendChild(table);

function displayName(tableCell){
	bookTitle.innerHTML = tableCell.innerHTML;
}
