let notes = (function() {
    let list = [];

    return {
        add: function(note) {
            if(note && typeof note == 'string' && note.trim().length != 0) {
                let item = {timestamp: Date.now(), text: note};
                list.push(item);
                return true;
            }
            return false;
        },
        remove: function(index) {
            if(list.length > index && (index === 0 || index)){
                list.splice(index, 1);
                return true;
            }
            return false;
        },
        count: function() {
            return list.length;
        },
        list: function() {

        },
        find: function(str) {
            if(str && typeof str == 'string' && str.trim().length != 0){
                for(let i = 0; i < list.length; i++){
                    if(list[i].text.localeCompare(str) == 0){
                        return list[i].text;
                    }
                }
            }
            return false;
        },
        clear: function() {
            list.splice(0, list.length);
        }
    }
}());