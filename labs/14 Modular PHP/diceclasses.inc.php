<?php

class Dice {
    private  $faces;
    private  $freqs = array();
    private  $p;
    private  $numargs;
    
    // Constructor
    public function __construct($faces, $p) {
        $this->numargs = func_num_args();
        if($this->numargs == 2) {
            $this->faces = $faces;
            $this->p = $p;
        } else{
            $this->faces = $faces;
        }
    }
    
    public function cast() {

        if($this->numargs == 2){
            if($this->p*100 <= rand(0, 100)){
                $res = $this->faces;
            }else{
                $res = rand(1,$this->faces - 1);
            }
        }else{
            $res = rand(1,$this->faces);
        }
        $this->freqs[$res]++;
        return $res;
    }
    
    public function getFreq($eyes) {
        $freq = $this->freqs[$eyes];
        if ($freq=="")
            $freq = 0;
        return $freq;
    }

    public function getAvgFace(){
        $avg = 0;
        for($i = 1 ; $i <= sizeof($this->freqs); $i++){
            $avg += $this->freqs[$i]*$i;
        }
        return $avg;
    }
}

class PhysicalDice extends Dice {
    private $material;

    public function __construct($material, $faces, $p)
    {
        $this->material = strvar($material);
        parent::__construct($faces, $p);
    }
}
?>