<?php

include("diceclasses.inc.php");

$material = $_GET["material"];
$faces = $_GET["faces"];
$q = $_GET["q"];
$throws = $_GET["throws"];

$results = array();

// make dice

$dice = new Dice($faces, $q);
for ($i = 1; $i<=$throws; $i++) {
    $res = $dice->cast();
    $results[] = array('id' => strval($i), 'res' => strval($res));
}
$freqs = array();
for ($i = 1; $i<=$faces; $i++) {
    $freqs[] = array ('eyes' => strval($i), 'frequency' => strval($dice->getFreq($i)));
}
$avg = strval($dice->getAvgFace());
echo json_encode(array('faces'=>$faces,'results'=>$results,'frequencies'=>$freqs, 'average'=> $avg));



?>