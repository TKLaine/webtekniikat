<?php
class Controller {
    private $model;

    public function __construct() {
        $this->model = new Model();
    }

    public function list_it() {
        $this->messages = $this->model->messages();
        include("View.php");
    }

    public function send() {
        if(isset($_POST["nick"])) {
            if(strlen($_POST["nick"])>1) {
                $_SESSION["user"] = $_POST["nick"];
            }
        }
        $this->model->add_message($_POST["message"]);
        header("location: Chat.php?action=list_it");
    }
}
